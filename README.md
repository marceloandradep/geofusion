## Instalation

Download repository.
```
git clone https://marceloandradep@bitbucket.org/marceloandradep/geofusion.git
```
Cd into repository folder.
```
cd geofusion
```
Build application package.
```
mvn package
```
Run application.
```
java -jar target\techchallenge-1.0.jar
```
You can also use
```
mvn spring-boot:run
```
to build and run the application with a single command.

## Running tests
To run the tests use
```
mvn test
```

## Deployed on AWS
You can access this service on AWS using
```
http://techchallenge-marceloandradep.boxfuse.io:8080
```