package br.com.geofusion.techchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechChallenge {
	
	public static void main(String[] args) {
		SpringApplication.run(TechChallenge.class, args);
	}

}
