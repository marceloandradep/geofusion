package br.com.geofusion.techchallenge.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import br.com.geofusion.techchallenge.configuration.properties.MessagingProperties;
import br.com.geofusion.techchallenge.configuration.properties.NotificationProperties;

@Configuration
@EnableConfigurationProperties({ MessagingProperties.class, NotificationProperties.class })
@EnableJms
public class AppConfiguration {

	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}

}
