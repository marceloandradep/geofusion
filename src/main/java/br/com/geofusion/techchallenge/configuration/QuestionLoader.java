package br.com.geofusion.techchallenge.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import br.com.geofusion.techchallenge.domain.Question;
import br.com.geofusion.techchallenge.repositories.QuestionRepository;

@Component
public class QuestionLoader {
	
	@Value("${questions.file}")
	private Resource questionFile;
	
	@Autowired
	private QuestionRepository repository;
	
	@PostConstruct
	public void load() throws IOException {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(questionFile.getInputStream()));
		
		buffer.lines()
			.filter(question -> !question.isEmpty())
			.map(question -> new Question(question))
			.forEach(question -> {
				repository.save(question);
			});
	}

}
