package br.com.geofusion.techchallenge.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.services.EmailCollectorService;

@RestController
@RequestMapping("/resources")
public class EmailCollectController {
	
	@Autowired
	private EmailCollectorService emailCollectorService;
	
	@RequestMapping(path = "/email", method = RequestMethod.POST)
	public void collectEmail(@RequestBody Subscriber subscriber) {
		emailCollectorService.collectEmail(subscriber);
	}

}
