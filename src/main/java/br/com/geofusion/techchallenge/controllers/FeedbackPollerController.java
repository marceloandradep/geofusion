package br.com.geofusion.techchallenge.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.services.FeedbackPollerService;

@RestController
@RequestMapping("/resources")
public class FeedbackPollerController {
	
	@Autowired
	private FeedbackPollerService service;
	
	@RequestMapping(path = "/feedback/{subscriberId}", method = RequestMethod.GET)
	public Subscriber findSubscriber(@PathVariable String subscriberId) {
		return service.findSubscriber(subscriberId);
	}
	
	@RequestMapping(path = "/feedback", method = RequestMethod.POST)
	public void saveSubscriber(@RequestBody Subscriber subscriber) {
		service.saveSubscriber(subscriber);
	}

}
