package br.com.geofusion.techchallenge.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Routes {
	
	@RequestMapping({
		"/feedback/{id}"
	})
	public String index() {
		return "forward:/index.html";
	}

}
