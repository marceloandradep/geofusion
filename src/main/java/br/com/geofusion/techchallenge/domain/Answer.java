package br.com.geofusion.techchallenge.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Answer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "questionId")
	private Question question;

	public Answer() {
		this(null, null);
	}
	
	public Answer(String description) {
		this(null, description);
	}
	
	public Answer(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public Answer(Question question) {
		this.question = question;
	}

	public Integer getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Question getQuestion() {
		return question;
	}
	
	public void setQuestion(Question question) {
		this.question = question;
	}
	
}
