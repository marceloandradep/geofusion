package br.com.geofusion.techchallenge.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Subscriber {
	
	@Id
	private String id;
	
	private String name;
	private String email;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "subscriberId")
	private List<Answer> answers;
	
	public Subscriber() {
		this(null, null);
	}
	
	public Subscriber(String name, String email) {
		this(UUID.randomUUID().toString(), name, email);
	}
	
	public Subscriber(String id, String name, String email) {
		this.id = id;
		this.name = name;
		this.email = email;
		
		answers = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	
	@JsonIgnore
	public Answer getLastAnswer() {
		if (this.answers.isEmpty()) {
			return null;
		}
		
		return this.answers.get(this.answers.size() - 1);
	}

	public boolean notAnsweredAnyQuestion() {
		return this.answers.isEmpty();
	}
	
	public void addUnansweredQuestion(Question question) {
		this.answers.add(new Answer(question));
	}
	
}
