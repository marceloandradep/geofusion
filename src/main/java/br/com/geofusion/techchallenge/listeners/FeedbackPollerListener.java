package br.com.geofusion.techchallenge.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.services.FeedbackPollerService;

@Component
public class FeedbackPollerListener {
	
	@Autowired
	private FeedbackPollerService service;
	
	@JmsListener(destination = "emailCollected")
	public void emailCollected(Subscriber subscriber) {
		service.saveSubscriber(subscriber);
	}

}
