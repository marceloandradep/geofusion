package br.com.geofusion.techchallenge.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.services.ResponderService;

@Component
public class ResponderListener {
	
	@Autowired
	private ResponderService responderService;
	
	@JmsListener(destination = "emailCollected")
	public void emailCollected(Subscriber subscriber) {
		responderService.respond(subscriber);
	}
	
}
