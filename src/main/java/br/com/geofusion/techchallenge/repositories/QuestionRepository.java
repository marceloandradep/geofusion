package br.com.geofusion.techchallenge.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.geofusion.techchallenge.domain.Question;

public interface QuestionRepository extends CrudRepository<Question, Integer> {
}
