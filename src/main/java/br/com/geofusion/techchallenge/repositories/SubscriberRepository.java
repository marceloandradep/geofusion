package br.com.geofusion.techchallenge.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.geofusion.techchallenge.domain.Subscriber;

public interface SubscriberRepository extends CrudRepository<Subscriber, String> {
}
