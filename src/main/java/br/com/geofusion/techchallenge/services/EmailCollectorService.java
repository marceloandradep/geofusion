package br.com.geofusion.techchallenge.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import br.com.geofusion.techchallenge.configuration.properties.MessagingProperties;
import br.com.geofusion.techchallenge.domain.Subscriber;

@Service
public class EmailCollectorService {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private MessagingProperties messagingProperties;
	
	public void collectEmail(Subscriber subscriber) {
		jmsTemplate.convertAndSend(messagingProperties.getTopic(), subscriber);
	}

}
