package br.com.geofusion.techchallenge.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.geofusion.techchallenge.domain.Question;
import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.repositories.QuestionRepository;
import br.com.geofusion.techchallenge.repositories.SubscriberRepository;

@Service
public class FeedbackPollerService {
	
	@Autowired
	private SubscriberRepository subscriberRepository;

	@Autowired
	private QuestionRepository questionRepository;
	
	public void saveSubscriber(Subscriber subscriber) {
		subscriberRepository.save(subscriber);
	}
	
	public Subscriber findSubscriber(String id) {
		Subscriber subscriber = subscriberRepository.findOne(id);
		
		if (subscriber.notAnsweredAnyQuestion()) {
			Iterable<Question> questions = questionRepository.findAll();
			
			for (Question question : questions) {
				subscriber.addUnansweredQuestion(question);
			}
		}
		
		return subscriber;
	}

}
