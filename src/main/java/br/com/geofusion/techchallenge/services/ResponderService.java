package br.com.geofusion.techchallenge.services;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import br.com.geofusion.techchallenge.configuration.properties.NotificationProperties;
import br.com.geofusion.techchallenge.domain.Subscriber;

@Service
public class ResponderService {
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private NotificationProperties notificationProperties;
	
	@Autowired
	private TemplatingService templatingService;
	
	public void respond(final Subscriber subscriber) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(subscriber.getEmail());
				message.setFrom(notificationProperties.getFrom());
				message.setSubject(notificationProperties.getSubject());
				
				Map<Object, Object> model = new HashMap<>();
				model.put("subscriber", subscriber);
				model.put("serviceHost", notificationProperties.getServiceHost());
				
				message.setText(templatingService.merge(notificationProperties.getTemplate(), model));
			}
		};
		
		mailSender.send(preparator);
	}

}
