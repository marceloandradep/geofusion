package br.com.geofusion.techchallenge.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

@Service
public class TemplatingService {
	
	@Autowired
	private Configuration configuration;
	
	public String merge(String template, Object model) {
		try {
			return FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(template), model);
		} catch (IOException | TemplateException e) {
			throw new RuntimeException("Error while processing template: " + template, e);
		}
	}

}
