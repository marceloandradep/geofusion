import { Component } from '@angular/core';
import SubscriptionComponent from './subscription/subscription.component';
import SurveyComponent from './survey/survey.component';
import { HTTP_PROVIDERS } from '@angular/http';
import {
  ROUTER_PROVIDERS,
  RouteConfig,
  ROUTER_DIRECTIVES
} from '@angular/router-deprecated';

@Component({
  selector: 'geofusion-app',
  directives: [SubscriptionComponent, SurveyComponent, ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS, ROUTER_PROVIDERS],
  templateUrl: 'app/app.component.html'
})
@RouteConfig([
  {
    path: '',
    name: 'SubscriptionComponent',
    component: SubscriptionComponent
  },
  {
    path: '/feedback/:id',
    name: 'SurveyComponent',
    component: SurveyComponent
  }
])
export default class AppComponent {}
