import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'geofusion-subscription',
  templateUrl: 'app/subscription/subscription.component.html'
})
export default class SubscriptionComponent {
  name: String;
  email: String;

  success: boolean;

  constructor(private http: Http) {
    this.success = false;
  }

  saveSubscription() {
    this.http.post('/resources/email', this)
    .subscribe(
      data => {
        this.success = true;
      },
      error => console.log(error)
    );
  }
};
