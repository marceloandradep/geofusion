import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { RouteParams } from '@angular/router-deprecated';

@Component({
  selector: 'geofusion-survey',
  templateUrl: 'app/survey/survey.component.html'
})
export default class SurveyComponent implements OnInit {
  subscriber: any;
  success: boolean;

  constructor(private http: Http, private routeParams: RouteParams) {
    this.success = false;
  }

  ngOnInit(): void {
    this.http.get("/resources/feedback/" + this.routeParams.get('id'))
      .map(response => response.json())
      .subscribe(
        subscriber => {
          this.subscriber = subscriber;
        },
        error => console.log(error)
      );
  }

  saveFeedback(): void {
    this.http.post('/resources/feedback', this.subscriber)
    .subscribe(
      data => {
        this.success = true;
      },
      error => console.log(error)
    );
  }
};
