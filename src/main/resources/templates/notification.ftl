<body>
	<h3>Hi, ${subscriber.name}!</h3>
	
	<p>We'd like to thank you for subscribing.</p>
	<p>We'd like you to know that our launch is scheduled to January 2016. 
	In the meantime let us know what do you expect from us!<p>
	
	<a href="${serviceHost}/feedback/${subscriber.id}">Give us your feedback!</a>
	
	<p>Thank you so much for your interest in Beer Tracker!</p>
</body>