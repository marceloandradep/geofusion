package br.com.geofusion.techchallenge.configuration;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geofusion.techchallenge.domain.Question;
import br.com.geofusion.techchallenge.repositories.QuestionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QuestionLoaderTest {
	
	@Autowired
	QuestionLoader questionLoader;
	
	@MockBean(name = "questionRepository")
	QuestionRepository questionRepository;
	
	@Test
	public void loading_test_questions_file() {
		then(questionRepository).should().save(new Question("Primeira?"));
		then(questionRepository).should().save(new Question("Segunda?"));
		then(questionRepository).should().save(new Question("Terceira?"));
		
		verifyNoMoreInteractions(questionRepository);
	}

}
