package br.com.geofusion.techchallenge.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.geofusion.techchallenge.configuration.properties.MessagingProperties;
import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.services.EmailCollectorService;

@RunWith(SpringRunner.class)
@WebMvcTest(EmailCollectController.class)
public class EmailCollectControllerTest {
	
	@MockBean
	private EmailCollectorService emailCollectorService;
	
	@MockBean
	private JmsTemplate jmsTemplate;
	
	@MockBean
	private MessagingProperties messagingProperties;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void subscription() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Subscriber subscriber = new Subscriber("Geofusion", "recipient@geofusion.com.br");
		
		mvc.perform(
				post("/resources/email")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(subscriber)))
		.andExpect(status().isOk());
		
		ArgumentCaptor<Subscriber> subscriberCaptor = ArgumentCaptor.forClass(Subscriber.class);
		then(emailCollectorService).should().collectEmail(subscriberCaptor.capture());
		
		subscriber = subscriberCaptor.getValue();
		
		assertThat(subscriber.getName()).isEqualTo("Geofusion");
		assertThat(subscriber.getEmail()).isEqualTo("recipient@geofusion.com.br");
	}

}
