package br.com.geofusion.techchallenge.controllers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.repositories.QuestionRepository;
import br.com.geofusion.techchallenge.repositories.SubscriberRepository;
import br.com.geofusion.techchallenge.services.FeedbackPollerService;
import br.com.geofusion.techchallenge.testutilities.builders.SubscriberBuilder;

@RunWith(SpringRunner.class)
@WebMvcTest(FeedbackPollerController.class)
public class FeedbackPollerControllerTest {
	
	@MockBean
	private FeedbackPollerService service;
	
	@MockBean
	private SubscriberRepository subscriberRepository;
	
	@MockBean
	private QuestionRepository questionRepository;
	
	@Value("classpath:br/com/geofusion/techchallenge/domain/subscriber.json")
	private Resource json;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void finding_a_subscriber() throws Exception {
		Subscriber subscriber = SubscriberBuilder.given()
				.subscriber("Tester Smith", "tester.smith@testco.com")
					.answered("Yes.").toQuestion("Why?").and()
					.answered("No.").toQuestion("Where?")
			.build();
		
		given(service.findSubscriber(anyString())).willReturn(subscriber);
		
		MvcResult result = mvc.perform(get("/resources/feedback/1"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();
		
		String expected = IOUtils.toString(json.getInputStream(), "utf-8");
		assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
}
