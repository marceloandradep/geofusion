package br.com.geofusion.techchallenge.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geofusion.techchallenge.testutilities.builders.SubscriberBuilder;

@RunWith(SpringRunner.class)
@JsonTest
public class SubscriberTest {
	
	@Autowired
	private JacksonTester<Subscriber> json;
	
	@Test
	public void json_serialization() throws IOException {
		Subscriber subscriber = SubscriberBuilder.given()
			.subscriber("Tester Smith", "tester.smith@testco.com")
				.answered("Yes.").toQuestion("Why?").and()
				.answered("No.").toQuestion("Where?")
		.build();
		
		assertThat(json.write(subscriber)).isEqualToJson("subscriber.json");
	}
	
	@Test
	public void getting_last_answer_when_there_is_no_answer() {
		Subscriber subscriber = SubscriberBuilder.given()
				.subscriber("Tester Smith", "tester.smith@testco.com").build();
		
		
		Answer lastAnswer = subscriber.getLastAnswer();
		
		assertThat(lastAnswer).isNull();
	}
	
	@Test
	public void getting_last_answer_when_there_is_some_answers() {
		Subscriber subscriber = SubscriberBuilder.given()
				.subscriber("Tester Smith", "tester.smith@testco.com")
					.answered("Yes.").toQuestion("Why?").and()
					.answered("No.").toQuestion("Where?")
			.build();
		
		Answer lastAnswer = subscriber.getLastAnswer();
		
		assertThat(lastAnswer.getDescription()).isEqualTo("No.");
		assertThat(lastAnswer.getQuestion().getDescription()).isEqualTo("Where?");
	}

}
