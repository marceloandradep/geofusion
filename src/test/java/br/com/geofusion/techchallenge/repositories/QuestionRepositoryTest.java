package br.com.geofusion.techchallenge.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geofusion.techchallenge.domain.Question;

@RunWith(SpringRunner.class)
@DataJpaTest
public class QuestionRepositoryTest {
	
	@Autowired
	private QuestionRepository questionRepository;
	
	@Test
	public void test() {
		Question question = new Question("A question?");
		
		questionRepository.save(question);
		
		question = questionRepository.findAll().iterator().next();
		
		assertThat(question).isNotNull();
		assertThat(question.getDescription()).isEqualTo("A question?");
	}

}
