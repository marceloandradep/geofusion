package br.com.geofusion.techchallenge.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geofusion.techchallenge.domain.Subscriber;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubscriberRepositoryTest {
	
	@Autowired
	private SubscriberRepository subscriberRepository;
	
	@Test
	public void test() {
		Subscriber subscriber = new Subscriber("Teste", "tester@junit.com");
		
		String id = subscriber.getId();
		
		subscriberRepository.save(subscriber);
		
		subscriber = subscriberRepository.findOne(id);
		
		assertThat(subscriber).isNotNull();
		assertThat(subscriber.getName()).isEqualTo("Teste");
		assertThat(subscriber.getEmail()).isEqualTo("tester@junit.com");
	}

}
