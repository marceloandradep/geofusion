package br.com.geofusion.techchallenge.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geofusion.techchallenge.domain.Question;
import br.com.geofusion.techchallenge.domain.Subscriber;
import br.com.geofusion.techchallenge.repositories.QuestionRepository;
import br.com.geofusion.techchallenge.repositories.SubscriberRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedbackPollerServiceTest {
	
	@Autowired
	private FeedbackPollerService service;
	
	@MockBean(name = "subscriberRepository")
	private SubscriberRepository subscriberRepository;

	@MockBean(name = "questionRepository")
	private QuestionRepository questionRepository;
	
	@Test
	public void populating_new_subscriber() {
		Subscriber subscriber = new Subscriber("id", "name", "email");
		
		given(subscriberRepository.findOne("id")).willReturn(subscriber);
		given(questionRepository.findAll()).willReturn(Arrays.asList(new Question("Will this test pass?")));
		
		subscriber = service.findSubscriber("id");
		
		assertThat(subscriber.getAnswers().size()).isEqualTo(1);
		assertThat(subscriber.getLastAnswer().getQuestion().getDescription()).isEqualTo("Will this test pass?");
	}

}
