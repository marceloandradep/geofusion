package br.com.geofusion.techchallenge.testutilities.builders;

import br.com.geofusion.techchallenge.domain.Subscriber;

public interface And {
	public WithAnswer and();
	public Subscriber build();
}