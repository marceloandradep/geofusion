package br.com.geofusion.techchallenge.testutilities.builders;

public interface Create {
	public WithAnswer subscriber(String name, String email);
}