package br.com.geofusion.techchallenge.testutilities.builders;

import br.com.geofusion.techchallenge.domain.Answer;
import br.com.geofusion.techchallenge.domain.Question;
import br.com.geofusion.techchallenge.domain.Subscriber;

public class SubscriberBuilder implements Create, WithAnswer, WithQuestion, And {
	
	private Subscriber subscriber;
	
	private SubscriberBuilder() {
	}
	
	public static Create given() {
		return new SubscriberBuilder();
	}
	
	@Override
	public WithAnswer subscriber(String name, String email) {
		subscriber = new Subscriber("1", name, email);
		return this;
	}
	
	@Override
	public WithQuestion answered(String answerDescription) {
		Answer answer = new Answer(subscriber.getAnswers().size(), answerDescription);
		subscriber.getAnswers().add(answer);
		return this;
	}
	
	@Override
	public And toQuestion(String questionDescription) {
		Answer lastAnswer = subscriber.getLastAnswer();
		
		Question question = new Question(lastAnswer.getId(), questionDescription);
		lastAnswer.setQuestion(question);
		
		return this;
	}
	
	@Override
	public WithAnswer and() {
		return this;
	}
	
	@Override
	public Subscriber build() {
		return subscriber;
	}
	
}
