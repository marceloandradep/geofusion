package br.com.geofusion.techchallenge.testutilities.builders;

import br.com.geofusion.techchallenge.domain.Subscriber;

public interface WithAnswer {
	public WithQuestion answered(String answerDescription);
	public Subscriber build();
}