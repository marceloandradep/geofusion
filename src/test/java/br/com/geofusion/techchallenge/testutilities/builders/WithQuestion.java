package br.com.geofusion.techchallenge.testutilities.builders;

public interface WithQuestion {
	public And toQuestion(String questionDescription);
}